# 安装包下载说明

#### 版本介绍
Linux下载安装包：EasyNVR-linux*****.tar.gz

Windows下载安装包：EasyNVR-window******.zip

onvif探测发现工具： ONVIF_Device_Test_Tool-14-06.zip

**软件包根目录包含使用文档、相关技术文档**

#### 版本下载通道

[在线API](http://demo.easynvr.com:10800/apidoc/ "在线API")

[官网](http://www.easynvr.com/)

QQ交流群：383501345

技术咨询：18855113030